\documentclass{article}

\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{dsfont}
\usepackage{amsthm}
\usepackage{todonotes}

\newtheorem{thm}{Théorème}
\newtheorem{defi}[thm]{Définition}
\newtheorem{rmk}[thm]{Remarque}


\usetikzlibrary{arrows,automata}
\usetikzlibrary{positioning}
\usetikzlibrary{trees}

\renewcommand{\phi}{\varphi}
\newcommand{\Z}{\mathds{Z}}
\newcommand{\N}{\mathds{N}}
\newcommand{\A}{\mathcal{A}}

\begin{document}

\author{Antoine \textsc{Dequay} \and Louis \textsc{Noizet}}
\title{Décidabilité de l'arithmétique de \bsc{Presburger}}
\date{\today}

\maketitle
\section*{Notes}
\begin{itemize}
    \item Prof : Nathalie \bsc{Bertrand} \& Nicolas \bsc{Markey}.
    \item Leçon : 909, 914, 924.
    \item Références :
    \begin{itemize}
        \item Carton, p.178.
    \end{itemize}
\end{itemize}

\newpage
\setcounter{page}{1}

\begin{defi}[Arithmétique de \bsc{Presburger}]
	On se place dans le langage de signature \(\{0,1,+,=\}\).
	L'arithmétique de Presburger correspond à la théorie de \(\N\) dans ce langage.
\end{defi}

\begin{defi}[Décidabilité]
Une théorie $T$ est dite décidable si le problème "une formule $\varphi$ est conséquence logique de $T$" est décidable.
\end{defi}

\begin{thm}
L'arithmétique de \bsc{Presburger} est décidable.
\end{thm}

\begin{proof}
	Pour montrer que l'arithmétique de Presburger est décidable, on va construire
	un automate $\A$ tel que : $\N\models\varphi[x_1=n_1,\dots,x_p=n_p]
	\Longleftrightarrow (n_1,\dots,n_p)\in L(\A)$.

Pour $k\in\N^*$, commençons par nous donner un alphabet pour représenter les $k$-uplets $(n_1,\dots,n_k)\in\N^k$. On pose $\Sigma_k=\{0,1\}^k$ et on représente les entiers en binaire avec le bit de poids faible à gauche. On ajoute des $0$ à droite pour avoir des représentations de la même taille, ce qui permet bien de représenter le $k$-uplet grâce à un mot sur $\Sigma_k$.

Soit $\varphi$ une formule sur la signature $\sigma$.

Sans que la sémantique ne soit changée, on remplace les sous-formule de $\varphi$ de la forme $\forall x\psi(x)$ par $\neg\exists x\neg\psi(x)$.

	On commence par décomposer les formules atomiques de \(\phi\) en une
	conjonction de formules du type \(x_i=x_j\), \(x_i+x_j=x_k\), \(x_i=0\) et \(x_i=1\)
Construisons par induction structurelle sur $\phi$ l'automate voulu.
\begin{itemize}
    \item Si $\phi$ est une formule atomique:
    \begin{itemize}
			\item Si $\phi$ est $x_i=x_j$ ou \(x_i+x_j=x_k\), on définit \(\A_\phi\)
				comme indiqué en figure~\ref{fig:atom}.
			\item Si \(\phi\) est \(x_i=0\) ou \(x_i=1\), on définit \(\A_\phi\) comme
				en figure~\ref{fig:constant}
					un automate de façon similaire.
    \end{itemize}
    \item Si $\phi$ est de la forme $\phi_1\wedge\phi_2$, on construit pour
			$\A_\phi$ l'automate de l'union de $\A_{\phi_1}$ et $\A_{\phi_2}$.
    \item Si $\phi$ est de la forme $\phi_1\vee\phi_2$, on construit pour
			$\A_\phi$ l'automate de l'intersection de $\A_{\phi_1}$ et $\A_{\phi_2}$.
    \item Si $\phi$ est de la forme $\neg\phi$, on construit pour $\A_\phi$
			l'automate du complémentaire (attention au coût de la déterminisation dans
			ce cas).
    \item Si $\phi$ est de la forme $\exists x\psi$, avec $x$ une variable
			non-libre, alors \(\vdash\phi\leftrightarrow\psi\) et on prend $\A_\phi=\A_{\psi}$.
    \item Si $\phi$ est de la forme $\exists x_i\psi$, et $\A_{\psi}=(Q,\delta,I,F)$ on pose $\A_\phi=(Q,\delta',I,F)$, avec $\delta'$ tel que :$$p\xrightarrow{(x_1,\dots,x_{k-1})}q\text{ dans }\A_\psi\Longleftrightarrow \exists x\text{ tel que }p\xrightarrow{(x_1,\dots,x_{k-1},x)}q\text{ dans }\A_{\psi_1}$$
\end{itemize}
\end{proof}

\begin{figure}
	\newcommand{\oi}[1]{\overset{i}{#1}}
	\newcommand{\oj}[1]{\overset{j}{#1}}
	\newcommand{\ok}[1]{\overset{k}{#1}}
		\begin{minipage}{.5\textwidth}
\begin{tikzpicture} [->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
    \tikzstyle{vertex}=[circle,draw = black, minimum size=17pt,inner sep=0pt]
    \node[state, accepting] (A) {};
    \node (I-) [node distance=1.5cm,left of=A] {};
    \path (I-) edge (A);
    \node[state] (P) [right of=A] {puits};
    \path (A) edge node [above] {$*$} (P);
    \path (A) edge [loop above] node {$\begin{array}{c}(\oi1,\oj1,*)\\(0,0,*)\end{array}$} (A);
    \path (P) edge [loop above] node {$*$} (P);
\end{tikzpicture}
\end{minipage}
		\begin{minipage}{.5\textwidth}
\begin{tikzpicture} [->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
    \tikzstyle{vertex}=[circle,draw = black, minimum size=17pt,inner sep=0pt]
    \node[state, accepting] (A) {0};
    \node (I-) [node distance=1.5cm,left of=A] {};
    \path (I-) edge (A);
		\node[state] (B) at (4,0) {1};
		\node[state] (P) at (2, -3) {puits};
		\path (A) edge [bend right] node [above] {$(\oi1,\oj1,\ok0,*)$} (B);
    \path (A) edge node [below left] {$*$} (P);
    \path (B) edge [bend right] node [above] {$(\oi0,\oj0,\ok1,*)$} (A);
    \path (B) edge node [below right] {$*$} (P);
    \path (A) edge [loop above] node
		{$\begin{array}{c}(\oi1,\oj0,\ok1,*)\\(0,1,1,*)\\(0,0,0,*)\end{array}$} (A);
    \path (B) edge [loop above] node
		{$\begin{array}{c}(\oi1,\oj1,\ok1,*)\\(1,0,0,*)\\(0,1,0,*)\end{array}$} (B);
    \path (P) edge [loop above] node {$*$} (P);
\end{tikzpicture}
\end{minipage}
		\caption{Les automates \(\A_{x_i=x_j}\) et \(A_{x_i+x_j=x_k}\)}
		\label{fig:atom}
\end{figure}

\begin{figure}
	\newcommand{\oi}[1]{\overset{i}{#1}}
		\begin{minipage}{.5\textwidth}
\begin{tikzpicture} [->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
    \tikzstyle{vertex}=[circle,draw = black, minimum size=17pt,inner sep=0pt]
    \node[state, accepting] (A) {};
    \node (I-) [node distance=1.5cm,left of=A] {};
    \path (I-) edge (A);
    \node[state] (P) [right of=A] {puits};
    \path (A) edge node [above] {$*$} (P);
		\path (A) edge [loop above] node {\((\oi0,*)\)} (A);
    \path (P) edge [loop above] node {$*$} (P);
\end{tikzpicture}
\end{minipage}
		\begin{minipage}{.5\textwidth}
\begin{tikzpicture} [->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
    \tikzstyle{vertex}=[circle,draw = black, minimum size=17pt,inner sep=0pt]
    \node[state] (A) {0};
    \node (I-) [node distance=1.5cm,left of=A] {};
    \path (I-) edge (A);
		\node[state, accepting] (B) at (4,0) {1};
		\node[state] (P) at (2,-3) {puits};
		\path (A) edge node [above] {$(\oi1,*)$} (B);
    \path (A) edge node [below left] {$*$} (P);
    \path (B) edge node [below right] {$*$} (P);
		\path (B) edge [loop above] node {\((\oi0,*)\)} (B);
    \path (P) edge [loop above] node {$*$} (P);
\end{tikzpicture}
\end{minipage}
		\caption{Les automates \(\A_{x_i=0}\) et \(A_{x_i=1}\)}
		\label{fig:constant}
\end{figure}

Ainsi, si \(\phi\) est une formule close, via cette induction structurelle, on
obtient un automate sur le langage vide, c'est-à-dire un automate avec
uniquement des \(\epsilon\)-transitions, et il suffit de vérifier que le mot
vide est accepté par cet automate. De manière équivalent, on peut voir cet
automate comme un graphe, et la forumle \(\phi\) est valide ssi il existe un
chemin d'un état initial vers un état acceptant.

Le problème d'acceptation d'un mot étant décidable, La théorie de \bsc{Presburger} est bien décidable.

\begin{rmk}
L'algorithme présenté ici est non-élémentaire
\end{rmk}

\begin{proof}
L'opération la plus coûteuse dans l'algorithme est la déterminisation d'un
	automate dans le cas de la négation. Comme une déterminisation fait passer
	d'un automate à $k$ états à un automate à $2^k$ états, si $\varphi$ contient
	$u$ symboles $\neg$ et $v$ symboles $\forall$, on doit appliquer un algorithme
	de déterminisation $u+2v$ fois lors de la construction de l'automate final.
	Cela a donc  un coût de $2\uparrow\uparrow(u+2v)$ dans le pire cas. Or
	\(u+2v\) dépend de l'entrée et n'est pas borné dans le cas général. Donc
	l'algorithme présenté ici est non-élémentaire.
\end{proof}

\begin{rmk}~
\begin{itemize}
	\item Le problème est en réalité dans $3$-\bsc{ExpTime}.
    \item L'arithmétique de \bsc{Presburger} est complète et cohérente.
\end{itemize}
\end{rmk}
\end{document}
