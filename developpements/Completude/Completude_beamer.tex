\documentclass[french]{beamer}

\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{soulutf8}

\usetheme{CambridgeUS}
\setbeamertemplate{navigation symbols}{}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{stmaryrd}
%
\newtheorem{thm}{Théorème}
\newtheorem{defi}[thm]{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
%
\newtheorem{exm}[thm]{Exemple}
\newtheorem{rmk}[thm]{Remarque}

\title{Théorème de complétude}
\author{Louis Noizet}
\date{}

\begin{document}

\begin{frame}
	\maketitle
\end{frame}

\section*{Remarques préalables}


\section{Contexte}

\begin{frame}{Règles de la déduction naturelle}

On rappelle les règles de la déduction naturelle.

	\[\inferrule*[right=(ax)]
		{~}
		{\Gamma,A\vdash A}
	\qquad
	\inferrule*[right=(w)]
		{\Gamma\vdash A}
		{\Gamma,\Delta\vdash A}\]

	\[\inferrule*[right=\ensuremath{(\rightarrow i)}]
	{\Gamma,A\vdash B}
	{\Gamma\vdash A\rightarrow B}
	\qquad
	\inferrule*[right=\ensuremath{(\rightarrow e)}]
	{\Gamma\vdash A\rightarrow B \\ \Gamma\vdash A}
	{\Gamma\vdash B}\]

	\[\inferrule*[right=\ensuremath{(\wedge i)}]
	{\Gamma\vdash A \\ \Gamma\vdash B}
	{\Gamma\vdash A\wedge B}
	\qquad
	\inferrule*[right=\ensuremath{(\wedge e_l)}]
	{\Gamma\vdash A\wedge B}
	{\Gamma\vdash A}
	\qquad
	\inferrule*[right=\ensuremath{(\wedge e_r)}]
	{\Gamma\vdash A\wedge B}
	{\Gamma\vdash B}\]

	\[\inferrule*[right=\ensuremath{(\vee i)}]
	{\Gamma\vdash A\text{~ou~}\Gamma\vdash B}
	{\Gamma\vdash A\vee B}
	\qquad
	\inferrule*[right=\ensuremath{(\vee e)}]
	{\Gamma\vdash A\vee B\\\Gamma, A\vdash C\\\Gamma, B\vdash C}
	{\Gamma\vdash C}\]


\end{frame}

\begin{frame}{Règles de la déduction naturelle (suite)}
\[\inferrule*[right=\ensuremath{(\neg i)}]
	{\Gamma,A\vdash\bot}
	{\Gamma\vdash \neg A}
	\qquad
\inferrule*[right=\ensuremath{(\neg e)}]
	{\Gamma\vdash A \\ \Gamma\vdash\neg A}
	{\Gamma\vdash\bot}\]

\[\inferrule*[right=\ensuremath{(r.a.a)}]
	{\Gamma,\neg A\vdash\bot}
	{\Gamma\vdash A}\]

\[\inferrule*[right=\ensuremath{(\exists i)}]
	{\Gamma\vdash A[x:=t]}
	{\Gamma\vdash \exists x\cdot A}
	\qquad
\inferrule*[right=\ensuremath{(\exists e)\text{, si }x\not\in\Gamma,C}]
	{\Gamma\vdash \exists x\cdot A \\ \Gamma,A\vdash C}
	{\Gamma\vdash C}\]

\[\inferrule*[right=\ensuremath{(\forall i)\text{, si }x\not\in\Gamma}]
	{\Gamma\vdash A}
	{\Gamma\vdash \forall x\cdot A}
	\qquad
\inferrule*[right=\ensuremath{(\forall e)}]
	{\Gamma\vdash \forall x\cdot A}
	{\Gamma\vdash A[x:=t]}\]

\end{frame}

\section{Preuve du théorème de complétude}

\begin{frame}
\begin{thm}[Complétude]\label{thm:completude}
	Soit \(\Gamma\) une théorie et \(F\) une formule close.
	\[\text{Si }\Gamma\vDash F\text{, alors }\Gamma\vdash F.\]
	En d'autres termes, si tous les modèles de \(\Gamma\) vérifient \(F\), alors
	on peut prouver \(F\) à partir de \(\Gamma\).
\end{thm}
\end{frame}

\newcommand\model{\ensuremath{\mathfrak{M}}}

\begin{frame}
Pour prouver le théorème de complétude, on se réduit au lemme suivant~:
\only<-2>{%
	\begin{lem}[\(\text{TC}_\text{max}\)]
		Pour toute théorie \(\Gamma\) maximale,
		\[\text{si }\Gamma\vDash\bot\text{ alors }\Gamma\vdash\bot.\]
		En d'autres termes et par contraposition, toute théorie maximale cohérente possède un modèle.
	\end{lem}
}
\uncover<2->{%
	\begin{lem}
		Si l'on a \(\text{TC}_\text{max}\), alors on a le théorème de complétude
	\end{lem}
}
	\only<3->{%
		\begin{proof}
			Soit \(\Gamma\) et \(F\) telles que
			\(\Gamma\vDash F\). Alors, \(\Gamma,\neg F\vDash\bot\).

			Supposons par l'absurde que \(\Gamma,\neg F\not\vdash\bot\). Alors \(\Gamma, \neg F\) est cohérente. Par le lemme de Zorn, il
			existe \(\Delta\supseteq\Gamma,\neg F\) telle que \(\Delta\) est
			maximale cohérente. Par hypothèse, \(\Delta\) possède un modèle \model.
			Mais alors \model{} est un modèle de \(\Gamma,\neg F\), ce qui
			contredit \(\Gamma,\neg F\vDash\bot\). Par (r.a.a), on a donc
			\(\Gamma\vdash F\).
		\end{proof}
		}
	\uncover<4>{On va donc montrer \(\text{TC}_\text{max}\).}
\end{frame}


\begin{frame}
	\begin{rmk}
		Soit \(T\) une théorie maximale cohérente. Pour toute formule close
		\(F\), on a les équivalences suivantes~: \[T\vdash F\Longleftrightarrow
		F\in T\] \[F\not\in T\Longleftrightarrow \neg F\in T \Longleftrightarrow
		T,F\vdash\bot\]
	\end{rmk}
\end{frame}


\newcommand\ol[1]{\overline{#1}}

\begin{frame}
	\begin{defi}[modèle des termes]
		Soit \(T\) une théorie. On définit \model{} le modèle des termes de \(T\)
		de la manière suivante~:
		\[\begin{cases}
			|\model|=\{\overline{t}\mid t\text{~termes clos}\}\\
			f_{\model}^i(\ol{t_1},\dots, \ol{t_n})=\ol{f^i(t_1,\dots,t_n)}\\
			R_{\model}^i(\ol{t_1},\dots, \ol{t_n})
			\Leftrightarrow R^i(t_1,\dots,t_n)\in T
		\end{cases}\]
	\end{defi}
\end{frame}


\begin{frame}
	\begin{lem}
		On a la complétude pour le calcul propositionnel
	\end{lem}
	\uncover<2->{
		\only<-2>{
		\begin{proof}
			Soit \(T\) une théorie maximale cohérente. Par
			\(\text{TC}_\text{max}\), il suffit de trouver un modèle à \(T\). Le
			modèle des termes fonctionne.  Pour prouver que c'est le cas, il faut
			et il suffit de montrer que pour toute formule \(F\), \[\model\vDash
			F\Leftrightarrow F\in T.\] On se contentera donc de montrer ce
			résultat par induction sur F.
			\renewcommand{\qedsymbol}{}
		\end{proof}
		}
			\only<3>{
		\begin{proof}
				\begin{itemize}[label=\(\bullet\)]
					\item Si \(F\) est atomique, le résultat est immédiat par
						définition de \model{}.
					\item Si \(F=\neg G\), on a~:
						\[\model\vDash\neg G
						\Leftrightarrow\model\not\vDash G
						\Leftrightarrow G\not\in T
						\Leftrightarrow\neg G\in T\]
					\item Si \(F=G\wedge H\), on a~:
						\[\model\vDash G\wedge H
						\Leftrightarrow\model\vDash G\text{ et }\model\vDash H
						\Leftrightarrow G\in T\text{ et }H\in T
						\Leftrightarrow F\in T\]
					\item Si \(F=G\vee H\), on a~:
						\[\model\vDash G\vee H
						\Leftrightarrow\model\vDash G\text{ ou }\model\vDash H
						\Leftrightarrow G\in T\text{ ou }H\in T
						\Leftrightarrow F\in T\]
				\end{itemize}
		\end{proof}
	}}
\end{frame}

\begin{frame}
\begin{rmk}
	On a presque la complétude du premier ordre. Il faudrait
	juste ajouter {\footnotesize
			\[\model\vDash \exists x\phi
			\overset{\text{def}}{\Leftrightarrow}
			\text{\(\exists t\) clos tq }\model\vDash\phi[x:=t]
			\overset{\text{HI}}{\Leftrightarrow}
			\text{\(\exists t\) clos tq }\phi[x:=t]\in T
			\overset{\text{1}}{\Leftrightarrow}
			\exists x \phi\in T\]
			\[\model\vDash \forall x\phi
			\overset{\text{def}}{\Leftrightarrow}
			\text{\(\forall t\) clos, }\model\vDash\phi[x:=t]
			\overset{\text{HI}}{\Leftrightarrow}
			\text{\(\forall t\) clos, }\phi[x:=t]\in T
			\overset{\text{2}}{\Leftrightarrow}
			\forall x \phi\in T\]}

			\begin{itemize}[label=\(\bullet\)]
				\item \(\overset{def}{\Leftrightarrow}\) par définition,
				\item \(\overset{HI}{\Leftrightarrow}\) par induction
				\item \(\overset{1}{\Rightarrow}\) et \(\overset{2}{\Leftarrow}\) immédiats
				\item \(\overset{1}{\Leftarrow}\) et \(\overset{2}{\Rightarrow}\)
					faux dans le cas général.
			\end{itemize}
\end{rmk}
\end{frame}

\begin{frame}
\begin{defi}
	On dit que \(T\) a des témoins de Henkin dans \(T'\), si pour toute formule
	\(\exists x\cdot F\in T\), il existe un terme clos \(t\) tel que
	\(F[x:=t]\in T'\).
\end{defi}

\begin{rmk}
	Les deux flèches restantes sont immédiatement vraies si \(T\) a des témoins
	de Henkin (dans lui-même).
	\[
			\text{\(\exists t\) clos tq }\phi[x:=t]\in T
			\overset{\text{1}}{\Leftarrow}
			\exists x\cdot\phi\in T\]
			\[\text{\(\forall t\) clos, }\phi[x:=t]\in T
			\overset{\text{2}}{\Rightarrow}
			\forall x\cdot\phi\in T\]
			(par contraposition, 2 est équivalent à 1 pour \(\neg\phi\))
\end{rmk}
\end{frame}

\begin{frame}
	\begin{lem}[Lemme de Henkin]
		Tout théorie cohérente \(T_0\) dans un langage \(L_0\) s'étend en une
		théorie cohérente maximale \(T\) ayant des témoins de Henkin
		dans un langage \(L\) où \(L\) est simplement \(L_0\) auquel on a ajouté
		des symboles de constantes.
	\end{lem}

\uncover<2->{
	\only<-2>{
\begin{proof}
	Par Zorn, on étend \(T_0\) et \(T_0^m\) maximale cohérente. On pose
	Pour tout \(i\), on pose~:
	\[L_{i+1}=L_i\cup\{c_{\exists x\cdot F}\mid\exists x\cdot F\in T_i^m\}\]
	et on pose
	\[T_{i+1}=T_i\cup\{F[x:=c_{\exists x\cdot F}]\mid
	\exists x\cdot F\in T_i^m\}\]
	\(T_{i+1}\) reste cohérente et donc on peut l'étendre en \(T_{i+1}^m\)
	maximale.
	\renewcommand{\qedsymbol}{}
\end{proof}
}

	\only<3>{
\begin{proof}
	On a
	\[T_0 \subseteq T_0^m \subseteq
	T_1 \subseteq T_1^m \subseteq \dots
	T_i \subseteq T_i^m \subseteq \dots\]

	On pose \[T=\bigcup\limits_{i\in\mathds{N}}T_i=
	\bigcup\limits_{i\in\mathds{N}}T^m_i.\]
	Il nous reste à vérifier que \(T\) vérifie la conclusion du lemme.
	\renewcommand{\qedsymbol}{}
\end{proof}
}

	\only<4>{
\begin{proof}
	\begin{itemize}[label=\(\bullet\)]
		\item \(L = \cup_{i\in\mathds{N}}L_i\) est bien le langage \(L_0\) plus
			des symboles de constantes.
		\item \(T\) est cohérente sinon on aurait un \(k\) tel que \(T_k\)
			non-cohérente
		\item \(T\) est maximale sinon, on aurait un i tel que \(T_i^m\) non
			maximale.
		\item \(T\) a des témoins de Henkin puisque si \(\exists x\cdot F\in T\),
			alors on a un \(k\) tel que \(\exists x\cdot F\in T_k\) et donc
			\(F[x:=c_{\exists x F}]\in T_{k+1}\).
	\end{itemize}
\end{proof}
}
}
\end{frame}


\begin{frame}{Conclusion}
	\begin{lem}[\(\text{TC}_\text{max}\)]
		Toute théorie maximale cohérente possède un modèle.
	\end{lem}

\begin{proof}
	On se donne \(T_0\) maximale cohérente. On l'étend en une théorie \(T\)
	maximale cohérente avec témoins de Henkin. On applique le schéma de preuve
	décrit ci-dessus. On a alors \(\model\) un modèle de \(T\). Mais alors
	\(\model\) est \emph{a fortiori} un modèle de \(T_0\). Et donc \(T_0\)
	possède un modèle.
\end{proof}
\end{frame}

\end{document}
