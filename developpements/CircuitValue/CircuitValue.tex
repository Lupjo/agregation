\documentclass{article}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{amsmath}
\usepackage{enumitem} % enumerate
\usepackage{amssymb} % square
\usepackage{mathpartir}
\usepackage{todonotes}
\usepackage{stmaryrd}
\usepackage{booktabs}
\usepackage[french]{babel}

\newtheorem{thm}{Théorème}
\newtheorem{defi}{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{lemin}{Lemme}[thm]

\newtheorem{exm}{Exemple}
\newtheorem{rmk}[exm]{Remarque}
\newtheorem{exo}{Exercice}

\title{\textsc{Circuit-Value} est P-complet (916?)}
\author{Louis Noizet}
\date{}
\begin{document}
\maketitle

\section*{Remarques préalables}

La référence de ce développement est \emph{Computational Complexity} de
Christos H. Papadimitriou.


\section{Contexte}
On se donne un circuit booléen, c'est-à-dire un graphe
orienté acyclique
\(G=(V,E), E\subseteq V^2\) et un sommet distingué \(v_f\in V\) où chaque vertice \(v\in V\) est
\begin{itemize}
	\item Ou bien une fonction booléenne \(\wedge\), \(\vee\) ou \(\neg\)
	\item Ou bien \(\top\) ou \(\bot\)
\end{itemize}
On demande de plus que \(G\) respecte les propriétés suivantes~:
\begin{itemize}
	\item Si \(v\) est \(\wedge\) ou \(\vee\), alors \(v\) a exactement deux
		arêtes entrantes et au plus une arête sortante
	\item Si \(v\) est \(\neg\), alors \(v\) a exactement une
		arêtes entrante et au plus une arête sortante
	\item Si \(v\) est \(\top\) ou \(\bot\), alors \(v\) n'a aucune arête
		entrante et au plus une arête sortante
\end{itemize}

Le problème \textsc{Circuit-Value} est le suivant~: étant donné un circuit
booléen, en utilisant les règles booléennes classiques, déterminer la valeur
booléenne de \(v_f\).

On fixe \(\Sigma\) un langage.

On considérera des machines de Turing à une seule bande. Initialement, le mot lu
est écrit sur la bande. Cette bande peut être modifiée.
On a
\[M=(Q, \Sigma, \Gamma, \delta, q_0, q_a, q_r)\]
avec
\[\begin{cases}\Gamma\supseteq\Sigma\sqcup\{\triangleright, \square\}\\
q_0,q_a, q_r\in Q\\
\delta: Q\times\Gamma\rightarrow Q\times\Gamma\times\{-1,0,+1\}\end{cases}\]
\(q_a\) est l'état acceptant, et \(q_r\) et l'état refusant.
On demande aussi les propriétés habituelles pour que ça fonctionne~:
\begin{itemize}
	\item Si on lit \(\triangleright\), on va à droite à l'étape suivante
	\item Il n'y a pas de transition partant de \(q_a\) ou \(q_r\).
\end{itemize}

\section{\textsc{Circuit-Value} est P}

On fait un tri topologique de \(P\), puis on calcule dans l'ordre.
Le nombre d'arêtes est inférieur à 3 fois le nombre de sommets, le calcul se
fera donc en temps linéaire si l'on choisit une bonne représentation mémoire.

Le tri topologique étant lui-même linéaire, on
a un algo linéaire, et \emph{a fortiori} polynomial pour \textsc{Circuit-Value}.

\section{\textsc{Circuit-Value} est P-difficile}

\subsection{Table de calcul}

On se donne une machine \(M=(Q, \Sigma, \Gamma, \delta, q_0, q_a, q_r)\) qui
termine sur toutes ses entrées, et un mot \(w\in\Sigma^*\). On se donne
\(T\) tel que la machine \(M\) s'exécute sur \(w\) en moins de \(T\) étapes.

On suppose que \(M\) s'exécute sur \(w\) en \(U-1\) étapes, en donnant la suite de configurations~:
\[(q_0,w_0=w,p_0 = 0) \Rightarrow
(q_1, w_1, p_1) \Rightarrow^*
(q_{U-1}, w_{U-1}, p_{U-1})\]
Pour \(t\geq U\), on pose \((q_t,w_t,p_t)=(q_{U-1},w_{U-1},p_{U-1})\).
On note \(w_t=w_t^1\dots w_t^{|w_t|}\) pour tout \(t\).
On définit alors la table de calcul de \(M\) sur \(w\),
notée \({(C_{it})}_{0\leq i,t\leq T-1}\),
de la façon suivante (voir figure~\ref{fig:table})~:

\[C'_{ti}=
\begin{cases}
	\triangleright&i=0\\
	w_t^i&0<i\leq|w_t|\\
	\square& i>|w_t|\\
\end{cases}\]
C'est à dire que la ligne \(t\) de \(C'\) contient le début du mot
\(\triangleright\cdot w_t\cdot\square^\infty\).
On pose ensuite~:
\[C_{ti}=
\begin{cases}
	C'_{ti}&p_t\neq i\\
	{(C'_{ti})}_{q_t}&p_t = i
\end{cases}\]
Donc pour tous \(t,i\), \(C_{ti}\in\Gamma\sqcup(\Gamma\times Q)\)

\begin{figure}
	\center{}
	\(\begin{matrix}
		\toprule
		\triangleright_{q_0} & 1 & 0 & 0 & 0 & 1 & \square & \square & \square\\
		\triangleright & 1_{q_0} & 0 & 0 & 0 & 1 & \square & \square & \square\\
		\triangleright & 1 & 0_{q_0} & 0 & 0 & 1 & \square & \square & \square\\
		\triangleright & 1 & 0 & 0_{q_0} & 0 & 1 & \square & \square & \square\\
		\triangleright & 1 & 0 & 0 & 0_{q_0} & 1 & \square & \square & \square\\
		\triangleright & 1 & 0 & 0 & 0 & 1_{q_0} & \square & \square & \square\\
		\triangleright & 1 & 0 & 0 & 0 & 1 & \square_{q_0} & \square & \square\\
		\triangleright & 1 & 0 & 0 & 0 & 1_{q} & \square & \square & \square\\
		\triangleright & 1 & 0 & 0 & 0 & 1_{q_r} & \square & \square & \square\\
		\bottomrule
	\end{matrix}\)
	\caption{Table de calcul pour une machine de parité}\label{fig:table}
\end{figure}

\subsection{Réduction}

On se donne un problème polynômial \(L\subseteq\Sigma^*\). Il existe donc
\(n\in \mathds N\) et une machine de Turing à \(1\) bande
\(\mathcal M = (Q,\Sigma,\Gamma,\delta,q_0,q_a, q_r)\).
Il existe aussi une fonction polynômiale \(f\) telle que pour tout
\(w\in\Sigma^*\), l'exécution de \(w\) s'effectue en temps \(\leq f(|w|)\)
On suppose sans perte de généralité que la machine termine avec la tête de
lecture en position 0.

On note \({(T_{ti})}_{0\leq t,i<m }\) la table de calcul de \(M\) sur \(w\).
On cherche à se donner un circuit booléen qui puisse se calculer en espace
logarithmique. On remarque que la case \(T_{ti}\) est entièrement déterminée
par les cases \(T_{(t-1)(i-1)}\),\(T_{(t-1)i}\) et \(T_{(t-1)(i+1)}\).

On peut encoder \(T_{ti}\) sur \(p=(1+\log\Gamma+\log Q)\) bits (le premier bit dit
si la case contient la tête de lecture, les \(\log\Gamma\) bits suivants donnent la lettre,
les \(\log Q\) suivants donnent l'état le cas échéant).

Il existe donc un circuit \(C\) de taille indépendante de \(w\) qui calcule
\(T_{ti}\) à partir de ces trois données (il prend \(3p\) fils en entrée et
renvoie \(p\) fils en sortie, voir figure~\ref{fig:C}).

Il est important de remarquer pour la suite que la valeur donnée en
\(T_{(t-1)(i-1)}\) (resp \(i+1\)) n'a pas d'influence si elle ne contient pas
la tête de lecture au temps \(t-1\).

\begin{figure}
	\center{}
	\includegraphics[page=1,width=0.5\textwidth]{CircuitValue_figures}
	\caption{Circuit \(C\)}\label{fig:C}
\end{figure}

On considère le circuit \(R_w\) donné en figure~\ref{fig:circuit}. Chaque ligne
représente \(p\) fils. Le circuit est correct, car la valeur \(\triangleright\)
en haut et en bas est ignorée et n'a donc pas d'influence sur les valeurs de
sortie. On sait alors que le mot \(w\) est accepté par la machine \(M\)
si et seulement si \(T_{m0}\) contient l'état \(q_a\). Cette vérification se
fait par le circuit booléen \(D\) en temps constant vis-à-vis de \(w\).

On a donc bien opéré une réduction de notre problème initial au problème
\textsc{Circuit-Value}. Il suffit de montrer que cette réduction est en espace
logarithmique. Il suffit globalement pour générer \(R_w\) de savoir compter
jusqu'à \(m^2\), ce qui nécessite un espace \(2\log m\).

Donc \textsc{Circuit-Value} est P-difficile


\begin{figure}
	\center{}
	\includegraphics[page=2,width=.9\textwidth]{CircuitValue_figures}
	\caption{Circuit \(R_w\)}\label{fig:circuit}
\end{figure}

\end{document}
