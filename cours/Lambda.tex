\documentclass[a4paper,french,12pt]{article}

% French + UTF8 in input, and good fonts
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}

\usepackage{dsfont} % mathds
\usepackage{amsthm} % thm, defi, prop, …
\usepackage{enumitem} % enumerate
\usepackage{amsmath} % align
\usepackage{listings} % listing
\usepackage{mathpartir} % inferrule
\usepackage{stmaryrd}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{bussproofs}

\newtheorem{thm}{Théorème}[section]
\newtheorem{defi}[thm]{Définition}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{pte}[thm]{Propriété}
\newtheorem{coro}[thm]{Corollaire}
\newtheorem{lem}[thm]{Lemme}
\newtheorem{lemin}{Lemme}[thm]

\newtheorem{exm}[thm]{Exemple}
\newtheorem{nota}[thm]{Notation}
\newtheorem{rmk}[thm]{Remarque}
\newtheorem{exo}[thm]{Exercice}

\newcommand{\N}{\mathds{N}}
\newcommand{\eqdef}{\triangleq}
\newcommand{\op}[1]{\operatorname{#1}}
\newcommand{\subst}[3]{[{#3}/{#2}]{#1}}
\newcommand{\alpheq}{=_\alpha}
\newcommand{\beteq}{=_\beta}
\newcommand{\lam}[2]{\lambda{#1}\cdot{#2}}
\newcommand{\betaredd}{\rhd_{1\beta}}
\newcommand{\betared}{\rhd_{\beta}}
\renewcommand{\ll}{\llbracket}
\newcommand{\rr}{\rrbracket}
\newcommand{\ol}[1]{\overline{#1}}
\renewcommand{\tt}{\top}
\newcommand{\ff}{\bot}
\newcommand{\on}[1]{\operatorname{#1}}
\DeclareMathOperator{\gd}{gd}
\newcommand{\code}[1]{\ensuremath{\left\lceil{#1}\right\rceil}}
\newcommand{\m}[1]{\mathcal{#1}}

\title{Lambda-Calcul\\\normalsize{Pour l'agrégation de maths option D}}
\author{Alan Schmitt}
\date{2020-2021}

\begin{document}
\maketitle

On se basera sur le livre \emph{Lambda-calculus and combinators, an introduction} de Hindley et Seldin.

Le \(\lambda\)-calcul a été inventé dans les années 1930 par Alonso Church.

\section{Introduction}

On se donne un ensemble dénombrable \(X\) de variables.

\begin{defi}
	On définit l'ensemble des \(\lambda\)-termes selon cette grammaire, où \(x \in X\) désigne une variable quelconque~:
	\[M \Coloneqq x \mid M N \mid \lambda x\cdot M.\]
\end{defi}

\begin{nota}
	On notera~:
	\[M N P = (M N) P,\]
	\[\lambda x\cdot M N = \lambda x\cdot(M N),\]
	\[\lam{x_1\dots x_n}{M} = \lam{x_1}{\dots\lam{x_n}{M}}.\]

	On pose par exemple l'identité \(I\eqdef\lambda x\cdot x\).
\end{nota}

\begin{defi}
	La taille \(\op{lg}\) d'un terme est définie par induction~:
	\begin{itemize}
		\item \(\op{lg}(x)=1\),
		\item \(\op{lg}(M N)=\op{lg}(M)+\op{lg}(N)\),
		\item \(\op{lg}(\lambda x\cdot M)=1+\op{lg}(M)\).
	\end{itemize}
\end{defi}


\subsection{Variables libres et liées}

\begin{defi}
	On définit \(P\in M\) par~:
	\begin{itemize}
		\item \(P\in P\),
		\item \(P\in M N\) ssi \(P\in M\) ou \(P\in N\),
		\item \(P\in \lambda x \cdot M\) ssi \(P\in M\) ou \(P\equiv x\).
	\end{itemize}
\end{defi}

\begin{defi}
	Si \(\lambda x\cdot M \in P\), alors \(M\) est la \emph{portée} de
	\(\lambda x\).
\end{defi}

\begin{defi}
	Si \(x\in P\), une occurrence de \(x\) est~:
	\begin{itemize}
		\item \emph{liée}, si elle est dans la portée d'un \(\lambda x\),
		\item \emph{liée} et \emph{liante}, si c'est le \(x\) de \(\lambda x\),
		\item \emph{libre}, sinon.
	\end{itemize}
\end{defi}

\begin{defi}[Définition alternative] On peut aussi définir $FV(P)$ les \emph{variables libres} (free variables) du terme $P$ ainsi~:
	\[FV(x) = \{x\},\]
	\[FV(MN) = FV(M)\cup FV(N),\]
	\[FV(\lambda x\cdot M) = FV(M)\setminus\{x\}.\]
\end{defi}


\subsection{Substitutions et $\alpha$-conversion}

\begin{defi}[Substitution]
	\(\subst{M}{x}{N}\) est défini inductivement de la manière suivante~:
	\begin{itemize}
		\item \(\subst{x}{x}{N}\equiv N\),
		\item \(\subst{P Q}{x}{N}\equiv (\subst{P}{x}{N})(\subst{Q}{x}{N})\),
		\item \(\subst{(\lambda x\cdot P)}{x}{N}\equiv \lambda x \cdot P\),
		\item \(\subst{(\lambda y\cdot P)}{x}{N}\equiv \lambda y \cdot P\)
			quand \(y\neq x\) et \(x\not\in FV(P)\),
		\item \(\subst{(\lambda y\cdot P)}{x}{N}\equiv \lambda y \cdot
			\subst{P}{x}{N}\)
			quand \(y\neq x\), \(x\in FV(P)\) et \(x\not\in FV(N)\),
		\item \(\subst{(\lambda z\cdot P)}{x}{N}\equiv \lambda z \cdot
			\subst{(\subst{P}{y}{z})}{x}{N}\)
			quand \(y\neq x\), \(x\in FV(P)\), \(x\in FV(N)\) et \(z\not\in FV(P)\cup FV(N)\).
	\end{itemize}
\end{defi}

\begin{lem} On a les résultats suivants~:
	\begin{itemize}
		\item \(\subst{M}{x}{x}\equiv M\),
		\item \(x\not\in FV(M) \Rightarrow \subst{M}{x}{N}\equiv M\),
		\item \(x\in FV(M) \Rightarrow FV(\subst{M}{x}{N}) = FV(N)\cup
			(FV(M)\setminus\{x\})\),
		\item \(\op{lg}(\subst{M}{x}{y}) = \op{lg}(M)\).
	\end{itemize}
\end{lem}

\begin{defi}[\(\alpha\)-conversion]
	On définit la relation \(=_{\alpha}\) de la manière suivante~:
	\begin{itemize}
		\item \(\lambda x\cdot M =_{\alpha} \lambda y\cdot\subst{M}{x}{y}\),
		\item Si \(M\alpheq N\) alors \(\lam{x}{M}\alpheq\lam{x}{N}\),
		\item Si \(M\alpheq P\) et \(N\alpheq Q\) alors \(M N\alpheq P Q\),
		\item Si \(M\alpheq N\) et \(N\alpheq L\) alors \(M\alpheq L\).
	\end{itemize}
\end{defi}

\begin{lem}
	Si \(M\alpheq N\), alors \(FV(M) = FV(N)\). De plus, \(\alpheq\) est une
	relation d'équivalence\footnote{C'est à dire une relation binaire, réflexive, symétrique, transitive.}.
\end{lem}

\begin{lem}
	Si \(M\alpheq M'\) et \(N\alpheq N'\), alors
	\(\subst{M}{x}{N}\alpheq\subst{M'}{x}{N'}\).
\end{lem}

\subsection{\(\beta\)-réduction}

\begin{defi}[\(\beta\)-réduction en une étape]
	On définit la \(\beta\)-réduction (en une étape) \(\betaredd\) de la manière suivante~:
	\begin{itemize}
		\item \((\lam{x}{M})N\betaredd\subst{M}{x}{N}\),
		\item Si \(M\betaredd M'\), alors \(M N\betaredd M' N\),
		\item Si \(N\betaredd N'\), alors \(M N\betaredd M N'\),
		\item Si \(M\betaredd M'\), alors \(\lam{x}{M}\betaredd \lam{x}{M'}\).
	\end{itemize}
\end{defi}

\begin{defi}
	On définit des \emph{stratégies d'évaluation}. On ne réduit jamais sous les
	\(\lambda\).
	\begin{itemize}
		\item \emph{Appel par nom}~: on réduit la fonction jusque \(\lam{x}{M}\), et on
			fait la \(\beta\)-réduction.
		\item \emph{Appel par valeur}~: on réduit la fonction, on réduit l'argument, on
			fait la \(\beta\)-réduction
		\item \emph{Appel par nécessité}~: (Haskell). On fait de l'appel par nom, mais
			on \(\beta\)-réduit en mettant toutes les occurrences de \(x\) au même
			endroit. Ainsi, on ne le calcule qu'une fois.
	\end{itemize}
\end{defi}

\begin{defi}[\(\beta\)-réduction]
	On dit que \(P\betared Q\) si \(P (\betaredd\vee\alpheq)^* Q\).

	Attention à ne pas oublier la possibilité d'\(\alpha\)-renommer !
\end{defi}

\begin{exm}
	\[(\lam{x}{x}) M\betared M,\]
	\[(\lam{xy}{x}) M N\betared M,\]
	\[(\lam{xy}{y}) M N\betared N,\]
	\[(\lam{x}{xx})(\lam{x}{xx})\betaredd(\lam{x}{xx})(\lam{x}{xx}).\]
\end{exm}


\subsection{Formes-normales}

\begin{defi}[\(\beta\)-nf]
	Un terme est en \emph{\(\beta\)-forme normal}e (\(\beta\)-nf) si
	\(P\not\betaredd\).
	Si \(Q\betared P\not\betaredd\), on dit que \(P\) est \textbf{une} \emph{forme normale} de \(Q\).
\end{defi}

\begin{lem}
	Si \(M\betared M'\) et \(N\betared N'\), alors
	\(\subst{M}{x}{N}\betared\subst{M'}{x}{N'}\).
\end{lem}

\begin{thm}[Church-Rosser pour \(\betared\), confluence]
	Si \(P\betared M\) et \(P\betared N\), alors il existe \(T\) tel que
	\(M\betared T\) et \(N\betared T\).
\end{thm}

\begin{coro}
	Si \(P\) a pour forme normale \(M\) et \(N\), alors \(M\alpheq N\).
\end{coro}

\begin{defi}[\(beta\)-conversion]
	On dit que \(P\) est \emph{\(\beta\)-convertible} à \(Q\), et on note \(P\beteq
	Q\) si il existe \(P_0,\dots, P_n\) tel que \(P_0=P\), \(P_n=Q\), et pour
	tout \(i\in\ll0;n-1\rr\)
	(\(P_i\betared P_{i+1}\) ou \(P_{i+1}\betared P_{i}\) ou
	\(P_i\alpheq P_{i+1}\)).
\end{defi}

\begin{lem}
	Si \(M\beteq M'\) et \(N\beteq N'\), alors
	\(\subst{M}{x}{N}\beteq\subst{M'}{x}{N'}\)
\end{lem}

\begin{thm}[Church-Rosser pour \(\beteq\)]
	Si \(P\beteq Q\), alors il existe \(T\) tel que \(P\betared T\) et
	\(Q\betared T\).
\end{thm}

\begin{thm} On a les résultats suivants~:
	\begin{itemize}
		\item Si \(P\beteq Q\) et \(Q\) est une forme normale, alors \(P\betared Q\).
		\item Si \(P\beteq Q\) alors soit \(P\) et \(Q\) ont la même forme
			normale, soit ils n'en ont pas
		\item Si \(P\beteq Q\) et \(P\) et \(Q\) en \(\beta\)-nf, alors
			\(P\alpheq Q\)
	\end{itemize}
\end{thm}

\begin{rmk}[Combinateurs]
	On se donne \(K\) et \(S\) tel que \(K X Y \rightarrow X\) et \(S X Y Z
	\rightarrow X Z (Y Z)\), alors on a toute la puissance du
	\(\lambda\)-calcul.
\end{rmk}

La suite et fin du document contient trois développements pour la leçon 929, ``Lambda-calcul pur comme modèle de calcul. Exemples''.
% https://agreg-maths.fr/lecons/1152

\newpage
\subsection{Développement 1/3~:\\
	Les fonctions récursives totales sont représentables par des combinateurs du \(\lambda\)-calcul}

\begin{defi}
	Soit \(x\mapsto\ol{x}\) un encodage (non-trivial) des entiers dans le \(\lambda\)-calcul.
	Une fonction \(\phi\) de \(\N^n\rightarrow\N\) est représentée par \(X\) si
	\[X \ol{m_1}\dots\ol{m_n}\beteq\ol{\phi(m_1,\dots,m_n)}.\]
\end{defi}

\begin{defi}[combinateur]
	Un terme du \(\lambda\)-calcul est un combinateur si \(FV(X)=\emptyset\).
\end{defi}

\begin{defi}
	Les fonctions récursives totales~:
	\begin{itemize}
		\item possèdent la fonction constante \(0:\N^0\rightarrow\N\),
		\item possèdent la fonction successeur \(\sigma:x\mapsto x+1\),
		\item possèdent les projections,
		\item sont closes par composition, récursion et minimisation.
	\end{itemize}
\end{defi}

\begin{defi}[Projection]
	Pour \(n\geq 1\) et \(1\leq k\leq n\). On a~:
	\[\pi_k^n:\N^n\rightarrow\N\text{ tel que }\pi_k^n(m_1\dots,m_n)=m_k.\]
\end{defi}

\begin{defi}[Composition]
	Soit \(\psi:\N^p\rightarrow\N\) et
	\(\phi_1,\dots,\phi_p:N^n\rightarrow\N\). On a~
	\(\chi=\psi\circ(\phi_1,\dots,\phi_p):\N^n\rightarrow\N\) tel que
	\[\chi(m_1\dots,m_n)=
		\psi(\phi_1(m_1,\dots,m_n),\dots,\phi_p(m_1,\dots,m_n)).\]
\end{defi}

\begin{defi}[Récursion]
	Soit \(\psi:\N^n\rightarrow\N\), \(\phi:\N^{n+2}\rightarrow\N\), alors on a
	\(\chi:\N^{n+1}\rightarrow\N\) défini par~:
	\[\phi(i,m_1,\dots,m_n)=
	\begin{cases}\psi(m_1,\dots,m_n)&,i=0\\
	\phi(n,\chi(n,m_1,\dots,m_n),m_1,\dots,m_n)&,i=n+1\end{cases}\]
\end{defi}

\begin{rmk}[Encodage de Church des entiers]
	On prend \(\ol{n}=\lam{sz}{s^nz}\). On a alors~:
	\begin{itemize}
		\item \(\ol{0} = \lam{sz}{z}\),
		\item \(\ol\sigma = \lam{n}{\lam{sz}{s(nsz)}}\),
		\item \(\ol{\pi_k^n}=\lam{x_1\dots x_n}{x_k}\).
	\end{itemize}
	Note : il existe d'autres encodages, moins utilisés.
\end{rmk}

\begin{lem}
	\((\ol\sigma)(\ol n)\beteq\ol{\sigma n}=\ol{n+1}\).
\end{lem}
\begin{proof}
	\begin{align*}
		(\ol{\sigma})(\ol{n})&=
	\lam{n}{\lam{sz}{s(nsz)}}(\ol{n})\\
	&\beteq {\lam{sz}{s(\ol{n}sz)}}\\
		&= {\lam{sz}{s((\lam{s'z'}{s'^nz'})sz)}}\\
		&\beteq {\lam{sz}{s({s^nz})}}\\
		&= {\lam{sz}{s^{n+1}z}}\\
		&= \ol{n+1}\\
	\end{align*}
\end{proof}

\paragraph{Composition~:}
\[\ol{\chi}=\lam{x_1\dots x_n}{\ol{\psi}(\ol{\phi_1}x_1\dots x_n)\dots(\ol{\phi_p}x_1\dots x_n)}.\]

\begin{lem}
	\(\ol{\phi}\ol{m_1}\dots\ol{m_n}\beteq\ol{\phi(m_1,\dots,m_n)}\).
\end{lem}

\paragraph{Récursion~:}
\begin{thm}
	Il existe un combinateur\footnote{Pour la culture générale, sachez que c'est ce combinateur \(Y\) qui a donné son nom au site \texttt{https://ycombinator.com/} très prisé par les développeurs Américains.} \(Y\) tel que \(Y x\beteq x(Y x)\).
	% https://www.ycombinator.com/faq/#q41
\end{thm}
\begin{proof}
	\(Y\equiv U U\) avec \(U\equiv\lam{ux}{x (u u x)}\).
\end{proof}

\paragraph{Booléens~:}
\[\ol{\tt} = \lam{xy}{x},\]
\[\ol{\ff} = \lam{xy}{x},\]
\[\ol{\on{if}} = \lam{bx_1x_2}{b x_1 x_2},\]
\[\ol{\on{is}_{\on{zero}}} = \lam{n}{n \lam{x}{\ol{\ff}} \ol{\tt}}\].

\begin{lem}
	On note \(D=\lam{xyz}{z(Ky)x}\). Alors
	\[
		\begin{cases}
			DMN\ol{0}\betared M,\\
			DMN(\ol{k+1})\betared N.
		\end{cases}
	\]
\end{lem}

Idée du prédécesseur~:
\[<0,0> \rightarrow <1,0> \rightarrow <2,1> \rightarrow \dots \rightarrow <n,n-1>.\]

\begin{lem}
	On pose \(<M,N>=\lam{c}{cMN}, \on{fst}=\ol\tt, \on{snd}=\ol\ff\)

	On pose \(\on{pred} = \lam{n}{\on{snd}(n(\lam{p}{<\ol\sigma(\on{fst} p), \on{fst}
	p>})<\ol0,\ol0>)}\)

	On a alors~:
	\[\on{pred}(\ol{k+1})\beteq\ol k.\]
\end{lem}
\begin{proof}
	Laissée à la lectrice.
\end{proof}

\begin{lem}[Récursion primitive]
Par récurrence, on va définir\\
\(\begin{cases}\phi(0, m_1,\dots, m_n) = \psi(m_1,\dots, m_n)\\
\phi(k+1, m_1,\dots, m_n) = \chi(k, \phi(k, m_1,\dots, m_n), m_1,\dots, m_n)\end{cases}\)

On suppose qu'on a une représentation \(\ol{\phi}\) de \(\phi\)
et une représentation \(\ol{\psi}\) de \(\psi\).

	On pose \(\ol\phi = Y F\) où
	\[F:=\lam{fkm_1\dots m_n}{D(\ol\psi m_1\dots m_n)(\ol\chi(\on{pred} k)(f
	(\on{pred} k)m_1\dots m_n)m_1\dots m_n) k}.\]

	Alors, pour tout \(k, m_1,\dots, m_n, m\), on a
	\[\phi(k,m_1,\dots,m_n) = m \leftrightarrow
	\ol\phi\ol{k}\ol{m_1}\dots,\ol{m_n} = \ol{m}. \]
\end{lem}

\begin{lem}[Minimisation]
	Si on a
	\[\phi(m_1,\dots,m_n)=\psi(\mu k[\chi(m_1,\dots,m_n,k)=0]),\]
	on pose ensuite
	\[F:=Y (\lam{fk} Dk(f(\ol\sigma k))(\ol\chi m_1\dots m_n k)).\]
	Et on pose enfin
	\[\ol\phi := \lam{m_1\dots m_n}{\ol\psi(F \ol 0)}\].

	On a alors~:
	\[\phi(m_1,\dots, m_n)=m \leftrightarrow \ol\phi\ol m_1\dots\ol m_n\beteq
	\ol m.\]
\end{lem}

\newpage
\subsection{Développement 2/3~: confluence de \(\betared\)}

Dans ce développement, on quotientera les \(\lambda\)-termes par
l'\(\alpha\)-équivalence.
On cherche à montrer la propriété de confluence de la \(beta\)-réduction suivante~:

\begin{thm}\label{thm:confluence}
	Si \(M\betared N\) et \(M\betared P\), alors il existe \(Q\) tel que
	\(N\betared Q\) et \(P\betared Q\).
\end{thm}

Attention, cela ne signifie pas que tout chemin de \(\beta\)-réductions depuis \(M\) arrivera à un unique terme \(Q\), mais que si deux dérivations commencent et atteignent deux termes \(N\) et \(P\), \emph{il est possible} de fermer le diagramme ``en diamant'' et d'atteindre un terme \(Q\) (voir
figure~\ref{fig:diamant}).

\begin{rmk}
	Si on remplace \(\betared\) par \(\betaredd\), la propriété est fausse. \\
	Par exemple en notant \(I=\lam{x}{x}\), on a
	\[
		\begin{cases}
			(\lam{x}{x x})(II)\betaredd
			II(II),\\
			(\lam{x}{x x})(II)\betaredd
			(\lam{x}{x x})I,\\
		\end{cases}
	\]
	mais on ne peut pas refermer le diamant en une seule étape de chaque côté.
\end{rmk}

\newcommand{\red}{\twoheadrightarrow_1}

\begin{nota}
	On commence par introduire la relation binaire \(\red\) définie par
	\begin{enumerate}[label=(\roman*)]
		\item\(\forall P, P\red P\),
		\item si \(P\red P'\), alors
				\(\lam{x}{P} \red \lam{x}{P'}\),
		\item si \(P\red P'\) et \(Q\red Q'\), alors
				 \(P Q\red P' Q'\),
		\item si \(P\red P'\) et \(Q\red Q'\), alors
			\((\lam{x}{P}) Q\red \subst{P'}{x}{Q'}\).
	\end{enumerate}
\end{nota}

\begin{lem}\label{lem:subst_ok}
	Si \(P\red P'\) et \(Q\red Q'\), alors
	\(\subst{P}{x}{Q}\red\subst{P'}{x}{Q'}\).
\end{lem}
\begin{proof}
	Admis.
\end{proof}

\begin{lem}
	\(\red\) respecte la propriété du diamant, i.e si \(M\red N\) et \(M\red P\), alors il existe
	\(Q\) tel que \(N\red Q\) et \(P\red Q\).
\end{lem}

\begin{proof}
	Soient \(M, M_1, M_2\) tels que \(M\red M_1\) et \(M\red M_2\). On cherche
	\(M_3\) tel que \(M_1\red M_3\) et \(M_2\red M_3\).
	On prouve par induction structurelle sur \(M\red M_1\) la propriété
	suivante~:
	\[(M\red M_1) \rightarrow
	\forall M_2,(M\red M_2) \rightarrow
	\exists M_3, (M_1\red M_3)\wedge (M_2\red M_3).\]
	\begin{enumerate}[label=(\roman*)]
		\setcounter{enumi}{1-1}
		\item Si \(M\red M_1\) est de la forme \(P\red P\), alors \(M=M_1\)
			et donc on peut poser \(M_3=M_2\).
		\setcounter{enumi}{2-1}
		\item Si \(M\red M_1\) est de la forme
			\(\lam{x}{P} \red \lam{x}{P'}\) avec \(P\red P'\), alors on a
				\(M=\lam{x}{P}\red M_2\). Nécessairement, on a \(M_2=\lam{x}{M'_2}\)
				avec \(M'\red M'_2\). Par hypothèse d'induction, on a donc \(M'_3\)
				tel que \(M'_1\red M'_3\) et \(M'_2\red M'_3\). On pose alors
				\(M_3=\lam{x}{M'_3}\).
		\setcounter{enumi}{4-1}
		\item Si \(M\red M_1\) est de la forme
			\((\lam{x}{P}) Q\red \subst{P'}{x}{Q'}\), avec \(P\red P'\) et
			\(Q\red Q'\)~:
			\begin{itemize}
				\item Soit \(M_2\) est de la forme \(\subst{P''}{x}{Q''}\) avec
					\(P\red P''\) et \(Q\red Q''\) Et donc par hypothèse d'induction on
					a \(P_3\) et \(Q_3\) vérifiant
					\(P'\red P_3\), \(P''\red P_3\), \(Q'\red Q_3\) et \(Q''\red Q_3\).
					On pose alors \(M_3=\subst{P_3}{x}{Q_3}\).
					Par le lemme~\ref{lem:subst_ok}, on a bien
					\(M_1\red M_3\) et \(M_2\red M_3\).
				\item Soit \(M_2\) est de la forme \((\lam{x}{P''}){Q''}\) avec
					\(P\red P''\) et \(Q\red Q''\). On conclut par induction.
			\end{itemize}
		\setcounter{enumi}{3-1}
	\item si \(M\red M_1\) est de la forme,
				 \(P Q\red P' Q'\), alors
			\begin{itemize}
				\item Soit \(M_2\) est de la forme \(P''Q''\) avec
					\(P\red P''\) et \(Q\red Q''\) et on peut conclure par hypothèse
					d'induction.
				\item Soit \(P=\lam{x}{P_1}\) et
					\(M_2\) est de la forme \(\subst{P_1''}{x}{Q''}\) avec
					\(P_1\red P_1''\) et \(Q\red Q''\). Alors, \(P'=\lam{x}{P_1'}\)
					avec \(P_1\red P_1'\).
					On applique l'hypothèse d'induction à \(P\) et \(Q\) et on obtient
					\(\lam{x}{P_3}\) et \(Q_3\). On pose \(M_3=\subst{P_3}{x}{Q_3}\).
			\end{itemize}
	\end{enumerate}
\end{proof}

\begin{lem}
	La clôture réflexive et transitive de \(\red\) est \(\betared\).
\end{lem}

\begin{proof}
	On rappelle que \(\betared\) est la clôture réflexive et transitive de \(\betaredd\), avec la possibilité d'\(\alpha\)-renommer.

	On a clairement \(\betaredd\subseteq\red\).
	Donc \(\betared\subseteq\red^*\)

	Réciproquement, par induction sur les règles définissant \(\red\), on peut
	vérifier que \(\red\subset\betared\). Par exemple, pour la deuxième règle,
	si \(\lam{x}{M}\red\lam{x}{M'}\), alors par hypothèse d'induction,
	\(M\betared M'\) donc \(\lam{x}{M}\betared\lam{x}{M'}\).

	Ainsi, \(\red\subseteq\betared\). Mais \(\betared\) est réflexive et
	transitive, donc \(\red^*\subseteq\betared\).
\end{proof}

Ainsi, \(\red\) respecte la propriété du diamant. Donc sa clôture aussi (voir
figure~\ref{fig:diamant}), ce qui conclut le théorème~\ref{thm:confluence}.

\begin{figure}
	\center
	\includegraphics[width=.7\textwidth]{diamant}
	% Cela peut ressembler à ça :
%	\begin{verbatim}
%
%      M
%	    /   \
%	   /     \
%	  /       P
% N       /
%	  \     /
%	   \   /
%     Q
%	\end{verbatim}
	\caption{Extension du ``diamant'' \(\diamond\) à la clôture transitive.}\label{fig:diamant}
\end{figure}

\newpage
\subsection{Développement 3/3~: L'ensemble des termes ayant une forme normale est
indécidable}

On se propose de prouver le théorème suivant~:

\begin{thm}
	L'ensemble des lambda-termes ayant une forme normale est indécidable.
\end{thm}

\begin{rmk}
	Une autre manière d'énoncer ce théorème est de dire que la terminaison d'un
	lambda-terme est indécidable.
\end{rmk}

\begin{rmk}
	Cet ensemble est clairement récursivement énumérable. Il n'est donc pas
	co-récursivement énumérable.
\end{rmk}

\begin{defi}
	Deux ensembles \(\m A\) et \(\m B\) sont \emph{récursivement séparables} ssi il existe
	une fonction \(\phi\) récursive totale dans \(\{0,1\}\) telle que
	\[\begin{cases}n\in \m A\Rightarrow\phi(n)=0\\
	n\in \m B\Rightarrow\phi(n)=1\end{cases}\]
\end{defi}

On suppose donné un encodage du \(\lambda\)-calcul
\(\gd:\Lambda\rightarrow\N\) et \(\tau, \nu\) récursives totales tels que
\[\tau(\gd(X),\gd(Y))=\gd(XY)\]
\[\nu(n)=\gd(\ol{n})\]

\begin{defi}
	On pose \(\code{X}=\ol{gd(X)}\).
\end{defi}

\begin{defi}
	Deux ensembles de termes \(\m A\) et \(\m B\) sont \emph{récursivement séparables} si
	\(\{\gd(X)\mid X\in \m A\}\) et \(\{\gd(X)\mid X\in \m B\}\) sont récursivement
	séparables.
\end{defi}

Un ensemble \(A\) est décidable si \(A\) et
\(A^C\) sont récursivement séparables.

\begin{defi}
	A est clos par \(\beteq\) si \(\forall A\in \m A\), \(\forall A'\beteq A\),
	alors \(A'\in \m A\)
\end{defi}

\begin{thm}[Scott-Curry]
	Aucune paire d'ensembles de termes clos par \(\beteq\) et non-vides et
	n'est récursivement séparable.
\end{thm}

\begin{proof}
	Soit \(\m A\) et \(\m B\) récursivement séparables, soit \(\phi\) la
	fonction récursive totale les séparant et \(F=\ol\phi\). Donc si \(X\in\m
	A\), alors \(\phi(\gd(X))=0\) donc
	\(F(\ol{\gd(x)})\beteq 0\), donc \(F(\code X)\beteq0\). De même, si
	\(X\in\m B\), alors \(F(\code X)\beteq 1\).

	Soit \(T = \ol\tau\).
	On a \(T(\ol{\gd(X)})(\ol{\gd(Y)})=\ol{\gd(XY)}\) i.e
	\(T\code{X}\code{Y}=\code{XY}\).

	Soit \(N = \ol\nu\).
	On a \(N(\ol n) = \ol{\gd(\ol{n})}=\code{\ol{n}}\)

	Soit \(A\in\m A\) et \(B\in\m B\).
	On pose
	\[H:=\lam{y}{DBA(F(Ty(Ny)))}\]
	Soit \(J=H\code{H}\)
	On a
	\begin{align*}
		J&=H\code{H}\\
		&\beteq DBA(F(T\code{H}(N\code{H})))\\
		&\beteq DBA(F(T\code{H}\code{\code{H}}))\\
		&\beteq DBA(F\code{H\code{H}})\\
		&= DBA(F\code{J}).
	\end{align*}

	\begin{itemize}
		\item Si \(\phi(\gd(J))=0\), alors \(F\code{J}\beteq\ol 0\) donc
		\(J\beteq B\) donc \(J\in\m B\) donc \(\phi(\gd(J))=1\).

		\item Si \(\phi(\gd(J))=1\), alors \(F\code{J}\beteq\ol 1\) donc
		\(J\beteq A\) donc \(J\in\m A\) donc \(\phi(\gd(J))=0\).
	\end{itemize}
\end{proof}

\begin{coro}
	\(\{X\mid X\text{ a une forme normale}\}\) n'est pas décidable.
\end{coro}

\begin{proof}
	\(\m A=\{X\mid X\text{ a une forme normale}\}\). Alors~:
	\begin{itemize}
		\item \(\m A\) est non-vide~: \(\lam{x}{x}\in\m A\),
		\item \(\m A\) est clos par \(\beteq\);
		\item \(\m A^C\) est non-vide~: \((\lam{x}{xx})(\lam{x}{xx})\in\m A^C\),
		\item \(\m A^C\) est clos par \(\beteq\).
	\end{itemize}
\end{proof}

\end{document}
