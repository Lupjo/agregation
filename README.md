# Agrégation de mathématiques - option D

Dans le dossier [developpements](developpements) vous trouverez les
développements d'informatique écrits pour préparer l'agrégation de mathématiques, option D,
à [l'université de Rennes 1](http://agreg-maths.univ-rennes1.fr/)
et [l'ÉNS de Rennes](http://www.dit.ens-rennes.fr/agregation-option-d/).

Leurs auteurs sont (selon les développements) Antoine Dequay, Julien Duron, Fabrice Étienne et Louis Noizet.

## Dossiers
- Dans le dossier [cours](cours) vous trouverez quelques cours qui nous ont été
  donnés et dont les professeurs ont accepté que nous partageions le contenu.

- Dans le dossier [utiles](utiles) se trouvent le rapport du jury de l'année 2019
  (les oraux 2020 n'ont pas eu lieu) et les programmes de 2021. Donc selon toute
  probabilité, ça ne sera pas utile aux générations futures.

- Dans le dossier [lecons](lecons), vous trouverez des plans de leçons, le plus
  souvent écrit à la main, et parfois également tapé en LaTeX.
